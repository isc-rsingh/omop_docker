/// datatransformation
/// from get patient info response to get patient location request
Class OMOP.Transform.saveEvent Extends OMOP.Common.DataTransform
{

ClassMethod Transform(source As OMOP.Proxy.eventItem, target As OMOP.Request.saveEventBO) As %Status
{

	s sc = $$$OK
	try {
		s target = ##class(OMOP.Request.saveEventBO).%New()
		
		s target.type = "saveEvent"
		
		s target.encounter = source.encounter
		s target.eventConceptId = source.eventConceptId
		s target.eventEndTime = source.eventEndTime
		s target.eventSouceValue = source.eventSouceValue
		s target.eventStartTime = source.eventStartTime
		s target.modifierConceptId = source.modifierConceptId
		s target.modifierSouceValue = source.modifierSouceValue
		s target.providerName = source.providerName
		s target.qualifierConceptId = source.qualifierConceptId
		s target.qualifierSouceValue = source.qualifierSouceValue
		s target.routeConceptId = source.routeConceptId
		s target.routeSouceValue = source.routeSouceValue
		s target.sourceTable = source.sourceTable
		s target.unitConceptId = source.unitConceptId
		s target.unitSouceValue = source.unitSouceValue
		s target.valueAsConceptId = source.valueAsConceptId
		s target.valueSouceValue = source.valueSouceValue
		s target.note = source.note
		s target.quantity = source.quantity
		

	} catch e {
		s sc = e.AsStatus()
		$$$TRACE(sc)
	}
	q sc
}

}
