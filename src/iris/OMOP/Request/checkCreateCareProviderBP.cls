Class OMOP.Request.checkCreateCareProviderBP Extends Ens.Request
{

Parameter RESPONSECLASSNAME = "OMOP.Response.getPatientLocationBP";

Property careProviderId As %String(MAXLEN = "", XMLNAME = "patientId");

Storage Default
{
<Data name="checkCreateCareProviderBPDefaultData">
<Subscript>"checkCreateCareProviderBP"</Subscript>
<Value name="1">
<Value>careProviderId</Value>
</Value>
</Data>
<DefaultData>checkCreateCareProviderBPDefaultData</DefaultData>
<Type>%Storage.Persistent</Type>
}

}
