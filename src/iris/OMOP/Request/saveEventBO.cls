Class OMOP.Request.saveEventBO Extends OMOP.Request.saveDataCommonBO
{

Parameter RESPONSECLASSNAME = "OMOP.Response.saveEventBO";

Property encounter As %String(MAXLEN = "", XMLNAME = "Encounter");

Property eventConceptId As %String(MAXLEN = "", XMLNAME = "event_concept_id");

Property eventSouceValue As %String(MAXLEN = "", XMLNAME = "event_souce_value");

Property eventStartTime As %String(MAXLEN = "", XMLNAME = "event_start_time");

Property eventEndTime As %String(MAXLEN = "", XMLNAME = "event_end_time");

Property providerName As %String(MAXLEN = "", XMLNAME = "provider_name");

Property qualifierConceptId As %String(MAXLEN = "", XMLNAME = "qualifier_concept_id");

Property qualifierSouceValue As %String(MAXLEN = "", XMLNAME = "qualifier_souce_value");

Property unitSouceValue As %String(MAXLEN = "", XMLNAME = "unit_souce_value");

Property unitConceptId As %String(MAXLEN = "", XMLNAME = "unit_concept_id");

Property modifierConceptId As %String(MAXLEN = "", XMLNAME = "modifier_concept_id");

Property modifierSouceValue As %String(MAXLEN = "", XMLNAME = "modifier_souce_value");

Property routeConceptId As %String(MAXLEN = "", XMLNAME = "route_concept_id");

Property routeSouceValue As %String(MAXLEN = "", XMLNAME = "route_souce_value");

Property valueAsConceptId As %String(MAXLEN = "", XMLNAME = "value_as_concept_id");

Property valueSouceValue As %String(MAXLEN = "", XMLNAME = "value_souce_value");

Property sourceTable As %String(MAXLEN = "", XMLNAME = "source_table");

Property personId As %String(MAXLEN = "", XMLNAME = "person_id");

Property note As %String(MAXLEN = "", XMLNAME = "note");

Property quantity As %String(MAXLEN = "", XMLNAME = "quantity");

Storage Default
{
<Data name="saveEventBODefaultData">
<Subscript>"saveEventBO"</Subscript>
<Value name="1">
<Value>encounter</Value>
</Value>
<Value name="2">
<Value>eventConceptId</Value>
</Value>
<Value name="3">
<Value>eventSouceValue</Value>
</Value>
<Value name="4">
<Value>eventStartTime</Value>
</Value>
<Value name="5">
<Value>eventEndTime</Value>
</Value>
<Value name="6">
<Value>providerName</Value>
</Value>
<Value name="7">
<Value>qualifierConceptId</Value>
</Value>
<Value name="8">
<Value>qualifierSouceValue</Value>
</Value>
<Value name="9">
<Value>unitSouceValue</Value>
</Value>
<Value name="10">
<Value>unitConceptId</Value>
</Value>
<Value name="11">
<Value>modifierConceptId</Value>
</Value>
<Value name="12">
<Value>modifierSouceValue</Value>
</Value>
<Value name="13">
<Value>routeConceptId</Value>
</Value>
<Value name="14">
<Value>routeSouceValue</Value>
</Value>
<Value name="15">
<Value>valueAsConceptId</Value>
</Value>
<Value name="16">
<Value>valueSouceValue</Value>
</Value>
<Value name="17">
<Value>sourceTable</Value>
</Value>
<Value name="18">
<Value>patientId</Value>
</Value>
<Value name="19">
<Value>personId</Value>
</Value>
<Value name="20">
<Value>note</Value>
</Value>
<Value name="21">
<Value>quantity</Value>
</Value>
</Data>
<DefaultData>saveEventBODefaultData</DefaultData>
<Type>%Storage.Persistent</Type>
}

}
