/// step 0
/// get patients list for future processing
Class OMOP.Operation.getPatientsList Extends Ens.BusinessOperation
{

Parameter ADAPTER = "EnsLib.SQL.OutboundAdapter";

Property Adapter As EnsLib.SQL.OutboundAdapter;

Parameter INVOCATION = "Queue";

Method getPatientInfo(pRequest As OMOP.Request.getPatientsList, Output pResponse As OMOP.Response.getPatientsList) As %Status
{
	s sc = $$$OK
	try{
		s sc = pRequest.NewResponse(.pResponse) 
		q:$$$ISERR(sc) 
		
		; artificial limitation
		; upload by sets of 10000 records
		; get last record ID
		
		// local table
		// no need to call external request
		// get last id from our storage
		set tRS=##class(%ResultSet).%New()
		
		set tSC=tRS.Prepare("SELECT TOP 1 pId FROM OMOP_Storage.PatientId "_
							"ORDER BY %id DESC")
		quit:$$$ISERR(tSC)
		set tSC=tRS.Execute() 
		quit:$$$ISERR(tSC)
		set cnt = 0
		
		set pId = ""
		while tRS.%Next() {
			set pId	= tRS.GetDataByName("pId")
		}
		; TOP 10000 
		s sql = "SELECT DISTINCT e.Patient "_
							"FROM HSAA.Patient p "_
    						"JOIN HSAA.Encounter as e ON p.id=e.patient "_
							"WHERE TagFacility='Baystate' "_
							
							; DISABLED FOR DOCKER ONLY
							;"AND Patient > "_+$g(pId)_" "_
							
							"ORDER BY e.Patient "
							
				
		; remote call
		;#dim rs As EnsLib.SQL.GatewayResultSet
		;s sc = ..Adapter.ExecuteQuery(.rs, sql)
		;q:$$$ISERR(sc) 
		
		; local call
		set rs=##class(%ResultSet).%New()
		Set sc=rs.Prepare(sql)
		quit:$$$ISERR(sc)

		set sc=rs.Execute()
		quit:$$$ISERR(sc)
		
		s recordNum = 0
		while rs.Next() {
			; DEBUG LIMIT 
			q:$i(recordNum)>100000
			d pResponse.patientsList.Insert($s(rs.GetDataByName("patient")'="":rs.GetDataByName("patient"),1:rs.GetDataByName("Patient")))
		}
		


		
	} catch e {
		s sc = e.AsStatus()
	}
	
	q sc
}

XData MessageMap
{
<MapItems>
  <MapItem MessageType="OMOP.Request.getPatientsList">
    <Method>getPatientInfo</Method>
  </MapItem>
</MapItems>
}

}
