/// 20211025 Denis Bulashev
/// step ...
/// collect information for event table
/// 
Class OMOP.Operation.getEventData Extends Ens.BusinessOperation
{

Parameter ADAPTER = "EnsLib.SQL.OutboundAdapter";

Property Adapter As EnsLib.SQL.OutboundAdapter;

Parameter INVOCATION = "Queue";

Method getPatientInfo(pRequest As OMOP.Request.getEventDataBO, Output pResponse As OMOP.Response.getEventDataBO) As %Status
{
	s sc = $$$OK
	try{
		s sc = pRequest.NewResponse(.pResponse) 
		q:$$$ISERR(sc) 
		
		s sql = "SELECT "_
				"Encounter, "_
				"Diagnosis_Code AS event_concept_id, "_
				"Diagnosis_Description AS event_souce_value, "_
				"COALESCE(DiagnosisTime, EnteredOn) AS event_start_time, "_
				"COALESCE(DiagnosisTime, EnteredOn) AS event_end_time,  "_
				"COALESCE(DiagnosisTime, EnteredOn) AS event_enteredon_date, "_
				"COALESCE(DiagnosingClinician_Code, '|', DiagnosingClinician_SDACodingStandard) AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"Diagnosis_Code AS value_as_concept_id, "_
				"Diagnosis_Description AS value_souce_value, "_
				"Null AS quantity, "_
				"'Diagnosis' AS source_table, "_
				"id AS source_id, "_
				"NULL AS note "_
				"FROM HSAA.Diagnosis "_
				"WHERE patient=? and (Diagnosis_Code IS NOT NULL OR Diagnosis_Description IS NOT NULL) and TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT "_
				"Null AS Encounter, "_
				"Allergy_Code AS event_concept_id, "_
				"Allergy_Description AS event_souce_value, "_
				"COALESCE(FromTime, ToTime) AS event_start_time, "_
				"COALESCE(ToTime, FromTime) AS event_end_time,  "_
				"EnteredOn AS event_enteredon_date, "_
				"COALESCE(Clinician_Code, '|', Clinician_SDACodingStandard) AS provider_name, "_
				"Severity_Code AS qualifier_concept_id, "_
				"Severity_Description AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"Allergy_Code AS value_as_concept_id, "_
				"Allergy_Description AS value_souce_value, "_
				"Null AS quantity, "_
				"'Allergy' AS source_table, "_
				"id AS source_id, "_
				"Null AS note "_
				"FROM HSAA.Allergy "_
				"WHERE patient=? and (Allergy_Code IS NOT NULL OR Allergy_Description IS NOT NULL) and TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT "_
				"Null AS Encounter, "_
				"Reaction_Code AS event_concept_id, "_
				"Reaction_Description AS event_souce_value, "_
				"COALESCE(FromTime, ToTime) AS event_start_time, "_
				"COALESCE(ToTime, FromTime) AS event_end_time,  "_
				"EnteredOn AS event_enteredon_date, "_
				"COALESCE(Clinician_Code, '|', Clinician_SDACodingStandard) AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"Reaction_Code AS value_as_concept_id, "_
				"Reaction_Description AS value_souce_value, "_
				"Null AS quantity, "_
				"'AllergyReaction' AS source_table, "_
				"id AS source_id, "_
				"Null AS note "_
				"FROM HSAA.Allergy "_
				"WHERE patient=? and (Reaction_Code IS NOT NULL or Reaction_Description IS NOT NULL) and Reaction_Code!=Allergy_Code and TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT  "_
				"Encounter,  "_
				"ObservationCode_Code AS event_concept_id, "_
				"ObservationCode_Description AS event_souce_value, "_
				"COALESCE(ObservationTime, EnteredOn) AS event_start_time, "_
				"COALESCE(ObservationTime, EnteredOn) AS event_end_time,  "_
				"COALESCE(ObservationTime, EnteredOn) AS event_enteredon_date, "_
				"COALESCE(Clinician_Code, '|', Clinician_SDACodingStandard) AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"ObservationCode_ObservationValueUnits_Code AS unit_concept_id, "_
				"ObservationCode_ObservationValueUnits_Description AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"ObservationRawValue AS value_as_concept_id, "_
				"ObservationRawValue AS value_souce_value, "_
				"Null AS quantity, "_
				"'Observation' AS source_table, "_
				"id AS source_id, "_
				"NULL AS note "_
				"FROM HSAA.Observation "_
				"WHERE patient=? and (ObservationCode_Code IS NOT NULL OR ObservationCode_Description IS NOT NULL) and TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT "_
				"Null AS Encounter,  "_
				"Problem_Code AS event_concept_id, "_
				"Problem_Description AS event_souce_value, "_
				"COALESCE(FromTime, ToTime) AS event_start_time, "_
				"COALESCE(ToTime, FromTime) AS event_end_time,  "_
				"EnteredOn AS event_enteredon_date, "_
				"COALESCE(Clinician_Code, '|', Clinician_SDACodingStandard)              AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"Problem_Code AS value_as_concept_id, "_
				"Problem_Description AS value_souce_value, "_
				"Null AS quantity, "_
				"'Problem' AS source_table, "_
				"id AS source_id, "_
				"NULL AS note "_
				"FROM HSAA.Problem "_
				"WHERE patient=? and (Problem_Code IS NOT NULL or Problem_Description IS NOT NULL) and TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT "_
				"Null AS Encounter,  "_
				"SocialHabit_Code AS event_concept_id, "_
				"SocialHabit_Description AS event_souce_value, "_
				"COALESCE(FromTime, ToTime) AS event_start_time, "_
				"COALESCE(ToTime, FromTime) AS event_end_time,  "_
				"EnteredOn AS event_enteredon_date, "_
				"COALESCE(EnteredBy_Code, '|', EnteredBy_SDACodingStandard)         AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"SocialHabitQty_Code AS value_as_concept_id, "_
				"SocialHabitQty_Description  AS value_souce_value, "_
				"Null AS quantity, "_
				"'SocialHistory' AS source_table, "_
				"id AS source_id, "_
				"NULL AS note "_
				"FROM HSAA.SocialHistory "_
				"WHERE patient=? and (SocialHabit_Code IS NOT NULL or SocialHabit_Description IS NOT NULL) and TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT "_
				"Null AS Encounter,  "_
				"HSAAProcedure_Code AS event_concept_id, "_
				"HSAAProcedure_Description AS event_souce_value, "_
				"COALESCE(ProcedureTime, EnteredOn) AS event_start_time, "_
				"COALESCE(ProcedureTime, EnteredOn) AS event_end_time,  "_
				"COALESCE(ProcedureTime, EnteredOn) AS event_enteredon_date,  "_
				"COALESCE(Clinician_Code, '|', Clinician_SDACodingStandard) AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"HSAAProcedure_Code AS value_as_concept_id, "_
				"HSAAProcedure_Description AS value_souce_value, "_
				"Null AS quantity, "_
				"'Procedure' AS source_table, "_
				"id AS source_id, "_
				"NULL AS note "_
				"FROM HSAA.HSAAProcedure "_
				"WHERE patient=? and (HSAAProcedure_Code IS NOT NULL or HSAAProcedure_Description IS NOT NULL) and TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT "_
				"o.Encounter AS Encounter,  "_
				"o.OrderCode_Code AS event_concept_id, "_
				"o.OrderCode_Description  AS event_souce_value, "_
				"COALESCE(m.FromTime, o.EnteredOn) AS event_start_time, "_
				"COALESCE(m.ToTime, o.EnteredOn) AS event_end_time,  "_
				"o.EnteredOn AS event_enteredon_date,  "_
				"COALESCE(o.OrderedBy_Code, '|', o.OrderedBy_SDACodingStandard)    AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"m.DoseUoM_Code AS unit_concept_id, "_
				"m.DoseUoM_Description AS unit_souce_value, "_
				"m.Route_Description AS route_souce_value, "_
				"Null AS value_as_concept_id, "_
				"Null AS value_souce_value, "_
				"m.DoseQuantity AS quantity, "_
				"'HSAA.Medication' AS source_table, "_
				"o.id AS source_id, "_
				"o.TextInstruction AS note "_
				"FROM HSAA.Medication as m "_
				"Inner join  "_
				"HSAA.HSAAOrder as o "_
				"on m.HSAAOrder=o.ID  "_
				"WHERE m.patient=? and (o.OrderCode_Code  IS NOT NULL or o.OrderCode_Description IS NOT NULL) and o.TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT "_
				"o.Encounter AS Encounter,  "_
				"o.OrderCode_Code AS event_concept_id, "_
				"o.OrderCode_Description  AS event_souce_value, "_
				"COALESCE(r.FromTime, o.EnteredOn) AS event_start_time, "_
				"COALESCE(r.ToTime, o.EnteredOn) AS event_end_time,  "_
				"o.EnteredOn AS event_enteredon_date,  "_
				"COALESCE(o.OrderedBy_Code, '|', o.OrderedBy_SDACodingStandard)  AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"Null AS value_as_concept_id, "_
				"Null AS value_souce_value, "_
				"Null AS quantity, "_
				"'LabResult' AS source_table, "_
				"o.id AS source_id, "_
				"o.TextInstruction AS note "_
				"FROM HSAA.LabResult as r "_
				"Inner join  "_
				"HSAA.HSAAOrder as o "_
				"on r.HSAAOrder=o.ID  "_
				"WHERE r.patient=? and (o.OrderCode_Code  IS NOT NULL or o.OrderCode_Description IS NOT NULL) and o.TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT "_
				"o.Encounter AS Encounter,  "_
				"o.OrderCode_Code AS event_concept_id, "_
				"o.OrderCode_Description  AS event_souce_value, "_
				"COALESCE(r.FromTime, o.EnteredOn) AS event_start_time, "_
				"COALESCE(r.ToTime, o.EnteredOn) AS event_end_time,  "_
				"o.EnteredOn AS event_enteredon_date,  "_
				"COALESCE(o.OrderedBy_Code, '|', o.OrderedBy_SDACodingStandard)  AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"Null AS value_as_concept_id, "_
				"Null AS value_souce_value, "_
				"Null AS quantity, "_
				"'OtherResult' AS source_table, "_
				"o.id AS source_id, "_
				"o.TextInstruction AS note "_
				"FROM HSAA.OtherResult as r "_
				"Inner join  "_
				"HSAA.HSAAOrder as o "_
				"on r.HSAAOrder=o.ID  "_
				"WHERE r.patient=? and (o.OrderCode_Code  IS NOT NULL or o.OrderCode_Description IS NOT NULL) and o.TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT "_
				"o.Encounter AS Encounter,  "_
				"o.OrderCode_Code AS event_concept_id, "_
				"o.OrderCode_Description  AS event_souce_value, "_
				"COALESCE(r.FromTime, o.EnteredOn) AS event_start_time, "_
				"COALESCE(r.ToTime, o.EnteredOn) AS event_end_time,  "_
				"o.EnteredOn AS event_enteredon_date,  "_
				"COALESCE(o.OrderedBy_Code, '|', o.OrderedBy_SDACodingStandard)  AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"Null AS value_as_concept_id, "_
				"Null AS value_souce_value, "_
				"Null AS quantity, "_
				"'RadResult' AS source_table, "_
				"o.id AS source_id, "_
				"r.ResultText AS note "_
				"FROM HSAA.RadResult as r "_
				"Inner join  "_
				"HSAA.HSAAOrder as o "_
				"on r.HSAAOrder=o.ID  "_
				"WHERE r.patient=? and (o.OrderCode_Code  IS NOT NULL or o.OrderCode_Description IS NOT NULL) and o.TagFacility='Baystate' "_
				" "_
				"UNION "_
				" "_
				"SELECT  "_
				"Encounter,  "_
				"DocumentType_Code AS event_concept_id, "_
				"DocumentType_Description AS event_souce_value, "_
				"COALESCE(DocumentTime, EnteredOn) AS event_start_time, "_
				"COALESCE(DocumentTime, EnteredOn) AS event_end_time,  "_
				"COALESCE(DocumentTime, EnteredOn) AS event_enteredon_date, "_
				"COALESCE(Clinician_Code, '|', Clinician_SDACodingStandard) AS provider_name, "_
				"Null AS qualifier_concept_id, "_
				"Null AS qualifier_souce_value, "_
				"Null AS unit_concept_id, "_
				"Null AS unit_souce_value, "_
				"Null AS route_souce_value, "_
				"Null AS value_as_concept_id, "_
				"DocumentName AS value_souce_value, "_
				"Null AS quantity, "_
				"'Document' AS source_table, "_
				"id AS source_id, "_
				"NoteText AS note "_
				"FROM HSAA.Document "_
				"WHERE patient=? and TagFacility='Baystate' "
		
		/*
		set sql = "SELECT Encounter, ObservationCode_Code AS event_concept_id,ObservationCode_Description AS event_souce_value,COALESCE(ObservationTime, EnteredOn) AS event_start_time,COALESCE(ObservationTime, EnteredOn) AS event_end_time, "_
			"COALESCE(ObservationTime, EnteredOn) AS event_enteredon_date, COALESCE(Clinician_Code, '|', Clinician_SDACodingStandard) AS provider_name, Null AS qualifier_concept_id, Null AS qualifier_souce_value, ObservationCode_ObservationValueUnits_Code AS unit_concept_id, "_
			"ObservationCode_ObservationValueUnits_Description AS unit_souce_value, Null AS route_souce_value, ObservationRawValue AS value_as_concept_id, ObservationRawValue AS value_souce_value, Null AS quantity, "_
			"'Observation' AS source_table, id AS source_id, NULL AS note FROM HSAA.Observation "_
			" WHERE patient=? and (ObservationCode_Code IS NOT NULL OR ObservationCode_Description IS NOT NULL) and TagFacility='Baystate'"_
			" UNION "_
			
			"SELECT "_
				"o.Encounter AS Encounter, o.OrderCode_Code AS event_concept_id, o.OrderCode_Description  AS event_souce_value, COALESCE(m.FromTime, o.EnteredOn) AS event_start_time, "_
				"COALESCE(m.ToTime, o.EnteredOn) AS event_end_time, o.EnteredOn AS event_enteredon_date, COALESCE(o.OrderedBy_Code, '|', o.OrderedBy_SDACodingStandard)    AS provider_name, "_
				"Null AS qualifier_concept_id, Null AS qualifier_souce_value, m.DoseUoM_Code AS unit_concept_id, m.DoseUoM_Description AS unit_souce_value, m.Route_Description AS route_souce_value, "_
				"Null AS value_as_concept_id, Null AS value_souce_value, m.DoseQuantity AS quantity, 'HSAA.Medication' AS source_table, o.id AS source_id, o.TextInstruction AS note "_
				"FROM HSAA.Medication as m Inner join HSAA.HSAAOrder as o on m.HSAAOrder=o.ID "_
				"WHERE m.patient=? and (o.OrderCode_Code  IS NOT NULL or o.OrderCode_Description IS NOT NULL) and o.TagFacility='Baystate'"_
			" UNION "_
			
			"SELECT Null AS Encounter, HSAAProcedure_Code AS event_concept_id, HSAAProcedure_Description AS event_souce_value, COALESCE(ProcedureTime, EnteredOn) AS event_start_time, COALESCE(ProcedureTime, EnteredOn) AS event_end_time, "_
			"COALESCE(ProcedureTime, EnteredOn) AS event_enteredon_date,  COALESCE(Clinician_Code, '|', Clinician_SDACodingStandard) AS provider_name, Null AS qualifier_concept_id, Null AS qualifier_souce_value, "_
			"Null AS unit_concept_id, Null AS unit_souce_value, Null AS route_souce_value, HSAAProcedure_Code AS value_as_concept_id, HSAAProcedure_Description AS value_souce_value, Null AS quantity, 'Procedure' AS source_table, "_
			"id AS source_id, NULL AS note FROM HSAA.HSAAProcedure "_
			"WHERE patient=? and (HSAAProcedure_Code IS NOT NULL or HSAAProcedure_Description IS NOT NULL) and TagFacility='Baystate'"
			*/
		
		s ^BUL1(1) = sql
				
		;$$$TRACE(sql)
		#dim rs As EnsLib.SQL.GatewayResultSet
		;s sc = ..Adapter.ExecuteQuery(.rs, sql, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId)
		;q:$$$ISERR(sc) 
		
		;for i=1:1:rs.GetColumnCount(){
		;	$$$TRACE(rs.GetColumnName(i))
		;}
		
		set rs = ##class(%ResultSet).%New()
		Set sc = rs.Prepare(sql)
		quit:$$$ISERR(sc)

		set sc = rs.Execute(pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId, pRequest.patientId)
		quit:$$$ISERR(sc)
		
		while rs.Next() {
			s eventItem = ##class(OMOP.Proxy.eventItem).%New()
			s eventItem.encounter = rs.GetDataByName("Encounter")
			s eventItem.eventConceptId = rs.GetDataByName("event_concept_id")
			s eventItem.eventSouceValue = rs.GetDataByName("event_souce_value")			
			s eventItem.eventStartTime = rs.GetDataByName("event_start_time")
			s eventItem.eventEndTime = rs.GetDataByName("event_end_time")
			s eventItem.eventEnteredonDate = rs.GetDataByName("event_enteredon_date")			
			s eventItem.providerName = rs.GetDataByName("provider_name")
			s eventItem.qualifierConceptId = rs.GetDataByName("qualifier_concept_id")
			s eventItem.qualifierSouceValue = rs.GetDataByName("qualifier_souce_value")			
			s eventItem.unitConceptId = rs.GetDataByName("unit_concept_id")
			s eventItem.unitSouceValue = rs.GetDataByName("unit_souce_value")
			s eventItem.routeSouceValue = rs.GetDataByName("route_souce_value")
			s eventItem.valueAsConceptId = rs.GetDataByName("value_as_concept_id")
			s eventItem.valueSouceValue = rs.GetDataByName("value_souce_value")
			s eventItem.quantity = rs.GetDataByName("quantity")
			s eventItem.sourceTable = rs.GetDataByName("source_table")
			s eventItem.sourceId =  rs.GetDataByName("source_id")
			s eventItem.note =  rs.GetDataByName("note")
			
			d pResponse.events.Insert(eventItem)
		}
		
		d rs.Close()
		
	} catch e {
		s sc = e.AsStatus()
	}
	
	q sc
}

XData MessageMap
{
<MapItems>
  <MapItem MessageType="OMOP.Request.getEventDataBO">
    <Method>getPatientInfo</Method>
  </MapItem>
</MapItems>
}

}
