Class OMOP.Response.checkCareSiteInIrisBO Extends Ens.Response
{

Property careSiteId As %String(MAXLEN = "", XMLNAME = "care_site_id");

Storage Default
{
<Data name="checkCareSiteInIrisBODefaultData">
<Subscript>"checkCareSiteInIrisBO"</Subscript>
<Value name="1">
<Value>ID</Value>
</Value>
<Value name="2">
<Value>Description</Value>
</Value>
<Value name="3">
<Value>AddressCityCode</Value>
</Value>
<Value name="4">
<Value>AddressStateCode</Value>
</Value>
<Value name="5">
<Value>AddressStreet</Value>
</Value>
<Value name="6">
<Value>AddressZipCode</Value>
</Value>
<Value name="7">
<Value>AddressCountryCode</Value>
</Value>
<Value name="8">
<Value>sourceValue</Value>
</Value>
<Value name="9">
<Value>careSiteId</Value>
</Value>
</Data>
<DefaultData>checkCareSiteInIrisBODefaultData</DefaultData>
<Type>%Storage.Persistent</Type>
}

}
