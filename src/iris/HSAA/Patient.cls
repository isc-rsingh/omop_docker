/// DO NOT EDIT - You should not directly alter this class in your applications.
/// 	HSAA.Indices.Patient
Class HSAA.Patient Extends %Persistent
{

// Allow synchronization

/// Property ID1 As %String(MAXLEN = "") [ SqlFieldName = ID ];
Property DeathTime As %String(MAXLEN = "");

Property Deceased As %String(MAXLEN = "");

Property BirthTime As %String(MAXLEN = "");

Property RaceCode As %String(MAXLEN = "") [ SqlFieldName = Race_Code ];

Property RaceDescription As %String(MAXLEN = "") [ SqlFieldName = Race_Description ];

Property EthnicGroupCode As %String(MAXLEN = "") [ SqlFieldName = EthnicGroup_Code ];

Property EthnicGroupDescription As %String(MAXLEN = "") [ SqlFieldName = EthnicGroup_Description ];

Property AddressStreet As %String(MAXLEN = "") [ SqlFieldName = Address_Street ];

Property AddressCityDescription As %String(MAXLEN = "") [ SqlFieldName = Address_City_Description ];

Property AddressCountRyCode As %String(MAXLEN = "") [ SqlFieldName = Address_CountRy_Code ];

Property AddressStateCode As %String(MAXLEN = "") [ SqlFieldName = Address_State_Code ];

Property AddressZipCode As %String(MAXLEN = "") [ SqlFieldName = Address_Zip_Code ];

Property GenderCode As %String(MAXLEN = "") [ SqlFieldName = Gender_Code ];

Property GenderDescription As %String(MAXLEN = "") [ SqlFieldName = Gender_Description ];

Property BirthGenderCode As %String(MAXLEN = "") [ SqlFieldName = BirthGender_Code ];

Property BirthGenderDescription As %String(MAXLEN = "") [ SqlFieldName = BirthGender_Description ];

Property RawGenderCode As %String(MAXLEN = "") [ SqlFieldName = RawGender_Code ];

Property RawGenderDescription As %String(MAXLEN = "") [ SqlFieldName = RawGender_Description ];

Property AddressCityCode As %String(MAXLEN = "") [ SqlFieldName = Address_City_Code ];

Property CreatedOn As %String(MAXLEN = "") [ SqlFieldName = CreatedOn ];

/// /
/// /Parameter DSTIME = "AUTO";
/// /
/// /Index HSAAIDIndex On HSAAID [ Unique ];
/// /
/// /Index MPIIDIndex On MPIID;
/// /
/// /Index BirthDateIndex On BirthDate;
/// /
/// /Index SSNOrProxyIndex On SSNOrProxy;
/// /
/// /Index CarrierSpecificMemberIDIndex On CarrierSpecificMemberID;
/// /
/// /Index FamilyNameIndex On Name.FamilyName;
/// /
/// /Index BirthTimeIndex On BirthTime;
/// /
/// /Index DeathTimeIndex On DeathTime;
/// /
/// /Index GenderCodeIndex On Gender.Code [ Type = bitmap ];
/// /
/// /// a negative POPORDER skips the setting of this property
/// /
/// //// HL7ToSDA3:<br>
/// //// PID-34.1 will be parsed to the EnteredAt Code if not null, else MSH-4.1<br>
/// //// will be. In either case, HL7ToSDA3 does not set the Description.<br><br>
/// //// Aggregation cache:<br>
/// //// The EnteredAt Code will always be set to the facility code of the best<br>
/// //// (most trusted) facility that a request goes out to. If the Access Manager<br>
/// //// has an InboundCodeSystemProfile specified, and that profile specifies an<br>
/// //// SDACodingStandard for Organization, then that value will be used. If the<br>
/// //// SDACodingStandard is filled in and there is a Description in the Code<br>
/// //// Registry for that Code and SDACodingStandard, that Description will be<br>
/// //// used.<br><br>
/// //// HL7:  PID-34 : LastUpdateFacility<br>
/// //// HL7:  MSH-4 : SendingFacility<br>
/// //// SDATableName=Organization, ViewerClass=User.HS.CTHospital<br>
/// //// .Code<br>
/// //// HL7:  PID-34.1 : LastUpdateFacility.NamespaceID<br>
/// //// HL7:  MSH-4.1 : SendingFacility.NamespaceID<br>
/// //// .Description<br>
/// //// .SDACodingStandard<br>
/// /Relationship EnteredAt2 As HSAA.EnteredAt(POPORDER = -1) [ Cardinality = one, Inverse = PatientRel ];
/// /
/// /Index EnteredAt2Index On EnteredAt2;
/// /
/// /Relationship AdvanceDirectiveRel As HSAA.AdvanceDirective [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship AllergyRel As HSAA.Allergy [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship AppointmentRel As HSAA.Appointment [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship CarePlanRel As HSAA.CarePlan [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship ClinicalRelationshipRel As HSAA.ClinicalRelationship [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship DiagnosisRel As HSAA.Diagnosis [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship DocumentRel As HSAA.Document [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship EncounterRel As HSAA.Encounter [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship EncounterGuarantorRel As HSAA.EncounterGuarantor [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship EncounterHealthPlanRel As HSAA.EncounterHealthPlan [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship EventCareProviderSiteRel As HSAA.EventCareProviderSite [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship FamilyHistoryRel As HSAA.FamilyHistory [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship GoalRel As HSAA.Goal [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship HealthConcernRel As HSAA.HealthConcern [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship IllnessHistoryRel As HSAA.IllnessHistory [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship InterventionRel As HSAA.Intervention [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship LabResultRel As HSAA.LabResult [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship LabResultItemRel As HSAA.LabResultItem [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship MedicalClaimRel As HSAA.MedicalClaim [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship MedicalClaimLineRel As HSAA.MedicalClaimLine [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship MedicalClaimLineHealthPlanRel As HSAA.MedicalClaimLineHealthPlan [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship MedicationRel As HSAA.Medication [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship MedicationAdministrationRel As HSAA.MedicationAdministration [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship MemberEnrollmentRel As HSAA.MemberEnrollment [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship MemberPCPSiteRel As HSAA.MemberPCPSite [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship MicrobiologyDetailRel As HSAA.MicrobiologyDetail [ Cardinality = many, Inverse = Patient ];
/// /
/// /// removed NextOfKin for now
/// /
/// /// Relationship NextOfKinRel As HSAA.NextOfKin [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship ObservationRel As HSAA.Observation [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship ObservationGroupRel As HSAA.ObservationGroup [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship OrderRel As HSAA.Order [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship OtherResultRel As HSAA.OtherResult [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship OutcomeRel As HSAA.Outcome [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship PatientNumberRel As HSAA.PatientNumber [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship PatientProgramRel As HSAA.PatientProgram [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship PhysicalExamRel As HSAA.PhysicalExam [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship ProcedureRel As HSAA.Procedure [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship ProblemRel As HSAA.Problem [ Cardinality = many, Inverse = Patient ];
/// /
/// //* &&&M:M 
/// /Relationship ProgramRel As HSAA.Program [ Cardinality = many, Inverse = Patient ];
/// /*/
/// /Relationship RadResultRel As HSAA.RadResult [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship ReferralRel As HSAA.Referral [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship SocialDeterminantRel As HSAA.SocialDeterminant [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship SocialHistoryRel As HSAA.SocialHistory [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship VaccinationRel As HSAA.Vaccination [ Cardinality = many, Inverse = Patient ];
/// /
/// /Relationship VitalSignRel As HSAA.VitalSign [ Cardinality = many, Inverse = Patient ];
/// /
/// /Method OnPopulate() As %Status [ Internal ]
/// /{
/// /	set sc = $$$OK
/// /	
/// /	// if the population package is included, call its method
/// /	if ##class(%Dictionary.CompiledClass).%ExistsId("HSAA.Populate.Patient") {
/// /		set sc = ##class(HSAA.Populate.Patient).Populate(##this)
/// /	}
/// /	else {
/// /		// since Patient has a computed age field, we do not want a deathtime
/// /		// before the birthtime 
/// /		set:(..DeathTime < ..BirthTime) ..DeathTime = ""
/// /	}
/// /	quit sc
/// /}
/// /
/// //// Delete PatientNumbers for Patient</br>
/// //// <var>sourceType</var> - type of source object - unused here</br>
/// //// <var>indent</var> - indentation level (for logging)</br>
/// /Method DeleteDependentObjects(sourceType As %String, indent As %String = "") As %Status
/// /{
/// /	set sc = $$$OK
/// /	try {
/// /	 	set id = ..%Id()
/// /		do ##class(HSAA.Logger).Info(indent_"Deleting patient numbers for patient "_id) 	
/// /
/// /		set stmt = "delete from HSAA.PatientNumber where patient =  ?"
/// /		set tResult = ##class(%SQL.Statement).%ExecDirect(, stmt, id)
/// /		set SQLCODE = tResult.%SQLCODE 
/// /		
/// /		if SQLCODE && (SQLCODE '= 100) {
/// /			set sc = $$$ERROR($$$HSAAErrRunSQL, stmt, SQLCODE) 
/// /			quit }	
/// /	}
/// /	catch ex {
/// /		set sc = ex.AsStatus()
/// /	}
/// /	quit sc
/// /}
Storage Default
{
<Data name="Numbers">
<Attribute>Numbers</Attribute>
<Structure>subnode</Structure>
<Subscript>"Numbers"</Subscript>
</Data>
<Data name="PatientDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
<Value name="2">
<Value>HSAAID</Value>
</Value>
<Value name="3">
<Value>MPIID</Value>
</Value>
<Value name="4">
<Value>Name</Value>
</Value>
<Value name="5">
<Value>Alias</Value>
</Value>
<Value name="6">
<Value>PrimaryLanguage</Value>
</Value>
<Value name="7">
<Value>Religion</Value>
</Value>
<Value name="8">
<Value>MaritalStatus</Value>
</Value>
<Value name="9">
<Value>Gender</Value>
</Value>
<Value name="10">
<Value>Race</Value>
</Value>
<Value name="11">
<Value>Ethnicity</Value>
</Value>
<Value name="12">
<Value>Occupation</Value>
</Value>
<Value name="13">
<Value>BirthTime</Value>
</Value>
<Value name="14">
<Value>BirthDate</Value>
</Value>
<Value name="15">
<Value>DeathTime</Value>
</Value>
<Value name="16">
<Value>Deceased</Value>
</Value>
<Value name="17">
<Value>IsMultipleBirth</Value>
</Value>
<Value name="18">
<Value>BirthOrder</Value>
</Value>
<Value name="19">
<Value>Age</Value>
</Value>
<Value name="20">
<Value>BirthPlace</Value>
</Value>
<Value name="21">
<Value>IsIdentityUnknown</Value>
</Value>
<Value name="22">
<Value>SetIsPregnant</Value>
</Value>
<Value name="23">
<Value>AccountNumber</Value>
</Value>
<Value name="24">
<Value>Address</Value>
</Value>
<Value name="25">
<Value>ContactInfo</Value>
</Value>
<Value name="26">
<Value>HealthPlan</Value>
</Value>
<Value name="27">
<Value>Tag</Value>
</Value>
<Value name="28">
<Value>Aliases</Value>
</Value>
<Value name="29">
<Value>RawGender</Value>
</Value>
<Value name="30">
<Value>SSNOrProxy</Value>
</Value>
<Value name="31">
<Value>EthnicGroup</Value>
</Value>
<Value name="32">
<Value>CarrierSpecificMemberID</Value>
</Value>
<Value name="33">
<Value>CommunicationPreferences</Value>
</Value>
<Value name="34">
<Value>CommunicationPreference</Value>
</Value>
<Value name="35">
<Value>Races</Value>
</Value>
<Value name="36">
<Value>Addresses</Value>
</Value>
<Value name="37">
<Value>Comments</Value>
</Value>
<Value name="38">
<Value>EnteredBy</Value>
</Value>
<Value name="39">
<Value>CreatedBy</Value>
</Value>
<Value name="40">
<Value>EnteredAt</Value>
</Value>
<Value name="41">
<Value>EnteredOn</Value>
</Value>
<Value name="42">
<Value>CreatedOn</Value>
</Value>
<Value name="43">
<Value>UpdatedOn</Value>
</Value>
<Value name="44">
<Value>LastUpdated</Value>
</Value>
<Value name="45">
<Value>PatientNumbers</Value>
</Value>
<Value name="46">
<Value>Extension</Value>
</Value>
<Value name="47">
<Value>BlankNameReason</Value>
</Value>
<Value name="48">
<Value>MothersMaidenName</Value>
</Value>
<Value name="49">
<Value>MothersFullName</Value>
</Value>
<Value name="50">
<Value>OtherLanguages</Value>
</Value>
<Value name="51">
<Value>CustomPairs</Value>
</Value>
<Value name="52">
<Value>SupportContacts</Value>
</Value>
<Value name="53">
<Value>IsProtected</Value>
</Value>
<Value name="54">
<Value>ProtectedEffectiveDate</Value>
</Value>
<Value name="55">
<Value>DeathLocation</Value>
</Value>
<Value name="56">
<Value>DeathDeclaredBy</Value>
</Value>
<Value name="57">
<Value>Citizenship</Value>
</Value>
<Value name="58">
<Value>PriorPatientNumbers</Value>
</Value>
<Value name="59">
<Value>FamilyDoctor</Value>
</Value>
<Value name="60">
<Value>InactiveMRNs</Value>
</Value>
<Value name="61">
<Value>PublicityCode</Value>
</Value>
<Value name="62">
<Value>PublicityEffectiveDate</Value>
</Value>
<Value name="63">
<Value>ImmunizationRegistryStatus</Value>
</Value>
<Value name="64">
<Value>ImmunizationRegistryStatusEffectiveDate</Value>
</Value>
<Value name="65">
<Value>EncounterNumber</Value>
</Value>
<Value name="66">
<Value>FromTime</Value>
</Value>
<Value name="67">
<Value>ToTime</Value>
</Value>
<Value name="68">
<Value>ExternalId</Value>
</Value>
<Value name="69">
<Value>Providers</Value>
</Value>
<Value name="70">
<Value>Organizations</Value>
</Value>
<Value name="71">
<Value>EnteredAt2</Value>
</Value>
<Value name="72">
<Value>BirthGender</Value>
</Value>
<Value name="73">
<Value>ID</Value>
</Value>
<Value name="74">
<Value>RaceCode</Value>
</Value>
<Value name="75">
<Value>RaceDescription</Value>
</Value>
<Value name="76">
<Value>EthnicGroupCode</Value>
</Value>
<Value name="77">
<Value>EthnicGroupDescription</Value>
</Value>
<Value name="78">
<Value>AddressStreet</Value>
</Value>
<Value name="79">
<Value>AddressCityDescription</Value>
</Value>
<Value name="80">
<Value>AddressCountRyCode</Value>
</Value>
<Value name="81">
<Value>AddressStateCode</Value>
</Value>
<Value name="82">
<Value>AddressZipCode</Value>
</Value>
<Value name="83">
<Value>GenderCode</Value>
</Value>
<Value name="84">
<Value>GenderDescription</Value>
</Value>
<Value name="85">
<Value>BirthGenderCode</Value>
</Value>
<Value name="86">
<Value>BirthGenderDescription</Value>
</Value>
<Value name="87">
<Value>RawGenderCode</Value>
</Value>
<Value name="88">
<Value>RawGenderDescription</Value>
</Value>
<Value name="89">
<Value>ID1</Value>
</Value>
<Value name="90">
<Value>AddressCityCode</Value>
</Value>
</Data>
<DataLocation>^HSAA.PatientD</DataLocation>
<DefaultData>PatientDefaultData</DefaultData>
<Description>
<![CDATA[/
/Parameter DSTIME = "AUTO";
/
/Index HSAAIDIndex On HSAAID [ Unique ];
/
/Index MPIIDIndex On MPIID;
/
/Index BirthDateIndex On BirthDate;
/
/Index SSNOrProxyIndex On SSNOrProxy;
/
/Index CarrierSpecificMemberIDIndex On CarrierSpecificMemberID;
/
/Index FamilyNameIndex On Name.FamilyName;
/
/Index BirthTimeIndex On BirthTime;
/
/Index DeathTimeIndex On DeathTime;
/
/Index GenderCodeIndex On Gender.Code [ Type = bitmap ];
/
/// a negative POPORDER skips the setting of this property
/
//// HL7ToSDA3:<br>
//// PID-34.1 will be parsed to the EnteredAt Code if not null, else MSH-4.1<br>
//// will be. In either case, HL7ToSDA3 does not set the Description.<br><br>
//// Aggregation cache:<br>
//// The EnteredAt Code will always be set to the facility code of the best<br>
//// (most trusted) facility that a request goes out to. If the Access Manager<br>
//// has an InboundCodeSystemProfile specified, and that profile specifies an<br>
//// SDACodingStandard for Organization, then that value will be used. If the<br>
//// SDACodingStandard is filled in and there is a Description in the Code<br>
//// Registry for that Code and SDACodingStandard, that Description will be<br>
//// used.<br><br>
//// HL7:  PID-34 : LastUpdateFacility<br>
//// HL7:  MSH-4 : SendingFacility<br>
//// SDATableName=Organization, ViewerClass=User.HS.CTHospital<br>
//// .Code<br>
//// HL7:  PID-34.1 : LastUpdateFacility.NamespaceID<br>
//// HL7:  MSH-4.1 : SendingFacility.NamespaceID<br>
//// .Description<br>
//// .SDACodingStandard<br>
/Relationship EnteredAt2 As HSAA.EnteredAt(POPORDER = -1) [ Cardinality = one, Inverse = PatientRel ];
/
/Index EnteredAt2Index On EnteredAt2;
/
/Relationship AdvanceDirectiveRel As HSAA.AdvanceDirective [ Cardinality = many, Inverse = Patient ];
/
/Relationship AllergyRel As HSAA.Allergy [ Cardinality = many, Inverse = Patient ];
/
/Relationship AppointmentRel As HSAA.Appointment [ Cardinality = many, Inverse = Patient ];
/
/Relationship CarePlanRel As HSAA.CarePlan [ Cardinality = many, Inverse = Patient ];
/
/Relationship ClinicalRelationshipRel As HSAA.ClinicalRelationship [ Cardinality = many, Inverse = Patient ];
/
/Relationship DiagnosisRel As HSAA.Diagnosis [ Cardinality = many, Inverse = Patient ];
/
/Relationship DocumentRel As HSAA.Document [ Cardinality = many, Inverse = Patient ];
/
/Relationship EncounterRel As HSAA.Encounter [ Cardinality = many, Inverse = Patient ];
/
/Relationship EncounterGuarantorRel As HSAA.EncounterGuarantor [ Cardinality = many, Inverse = Patient ];
/
/Relationship EncounterHealthPlanRel As HSAA.EncounterHealthPlan [ Cardinality = many, Inverse = Patient ];
/
/Relationship EventCareProviderSiteRel As HSAA.EventCareProviderSite [ Cardinality = many, Inverse = Patient ];
/
/Relationship FamilyHistoryRel As HSAA.FamilyHistory [ Cardinality = many, Inverse = Patient ];
/
/Relationship GoalRel As HSAA.Goal [ Cardinality = many, Inverse = Patient ];
/
/Relationship HealthConcernRel As HSAA.HealthConcern [ Cardinality = many, Inverse = Patient ];
/
/Relationship IllnessHistoryRel As HSAA.IllnessHistory [ Cardinality = many, Inverse = Patient ];
/
/Relationship InterventionRel As HSAA.Intervention [ Cardinality = many, Inverse = Patient ];
/
/Relationship LabResultRel As HSAA.LabResult [ Cardinality = many, Inverse = Patient ];
/
/Relationship LabResultItemRel As HSAA.LabResultItem [ Cardinality = many, Inverse = Patient ];
/
/Relationship MedicalClaimRel As HSAA.MedicalClaim [ Cardinality = many, Inverse = Patient ];
/
/Relationship MedicalClaimLineRel As HSAA.MedicalClaimLine [ Cardinality = many, Inverse = Patient ];
/
/Relationship MedicalClaimLineHealthPlanRel As HSAA.MedicalClaimLineHealthPlan [ Cardinality = many, Inverse = Patient ];
/
/Relationship MedicationRel As HSAA.Medication [ Cardinality = many, Inverse = Patient ];
/
/Relationship MedicationAdministrationRel As HSAA.MedicationAdministration [ Cardinality = many, Inverse = Patient ];
/
/Relationship MemberEnrollmentRel As HSAA.MemberEnrollment [ Cardinality = many, Inverse = Patient ];
/
/Relationship MemberPCPSiteRel As HSAA.MemberPCPSite [ Cardinality = many, Inverse = Patient ];
/
/Relationship MicrobiologyDetailRel As HSAA.MicrobiologyDetail [ Cardinality = many, Inverse = Patient ];
/
/// removed NextOfKin for now
/
/// Relationship NextOfKinRel As HSAA.NextOfKin [ Cardinality = many, Inverse = Patient ];
/
/Relationship ObservationRel As HSAA.Observation [ Cardinality = many, Inverse = Patient ];
/
/Relationship ObservationGroupRel As HSAA.ObservationGroup [ Cardinality = many, Inverse = Patient ];
/
/Relationship OrderRel As HSAA.Order [ Cardinality = many, Inverse = Patient ];
/
/Relationship OtherResultRel As HSAA.OtherResult [ Cardinality = many, Inverse = Patient ];
/
/Relationship OutcomeRel As HSAA.Outcome [ Cardinality = many, Inverse = Patient ];
/
/Relationship PatientNumberRel As HSAA.PatientNumber [ Cardinality = many, Inverse = Patient ];
/
/Relationship PatientProgramRel As HSAA.PatientProgram [ Cardinality = many, Inverse = Patient ];
/
/Relationship PhysicalExamRel As HSAA.PhysicalExam [ Cardinality = many, Inverse = Patient ];
/
/Relationship ProcedureRel As HSAA.Procedure [ Cardinality = many, Inverse = Patient ];
/
/Relationship ProblemRel As HSAA.Problem [ Cardinality = many, Inverse = Patient ];
/
//* &&&M:M 
/Relationship ProgramRel As HSAA.Program [ Cardinality = many, Inverse = Patient ];
/*/
/Relationship RadResultRel As HSAA.RadResult [ Cardinality = many, Inverse = Patient ];
/
/Relationship ReferralRel As HSAA.Referral [ Cardinality = many, Inverse = Patient ];
/
/Relationship SocialDeterminantRel As HSAA.SocialDeterminant [ Cardinality = many, Inverse = Patient ];
/
/Relationship SocialHistoryRel As HSAA.SocialHistory [ Cardinality = many, Inverse = Patient ];
/
/Relationship VaccinationRel As HSAA.Vaccination [ Cardinality = many, Inverse = Patient ];
/
/Relationship VitalSignRel As HSAA.VitalSign [ Cardinality = many, Inverse = Patient ];
/
/Method OnPopulate() As %Status [ Internal ]
/{
/	set sc = $$$OK
/	
/	// if the population package is included, call its method
/	if ##class(%Dictionary.CompiledClass).%ExistsId("HSAA.Populate.Patient") {
/		set sc = ##class(HSAA.Populate.Patient).Populate(##this)
/	}
/	else {
/		// since Patient has a computed age field, we do not want a deathtime
/		// before the birthtime 
/		set:(..DeathTime < ..BirthTime) ..DeathTime = ""
/	}
/	quit sc
/}
/
//// Delete PatientNumbers for Patient</br>
//// <var>sourceType</var> - type of source object - unused here</br>
//// <var>indent</var> - indentation level (for logging)</br>
/Method DeleteDependentObjects(sourceType As %String, indent As %String = "") As %Status
/{
/	set sc = $$$OK
/	try {
/	 	set id = ..%Id()
/		do ##class(HSAA.Logger).Info(indent_"Deleting patient numbers for patient "_id) 	
/
/		set stmt = "delete from HSAA.PatientNumber where patient =  ?"
/		set tResult = ##class(%SQL.Statement).%ExecDirect(, stmt, id)
/		set SQLCODE = tResult.%SQLCODE 
/		
/		if SQLCODE && (SQLCODE '= 100) {
/			set sc = $$$ERROR($$$HSAAErrRunSQL, stmt, SQLCODE) 
/			quit }	
/	}
/	catch ex {
/		set sc = ex.AsStatus()
/	}
/	quit sc
/}]]></Description>
<ExtentSize>0</ExtentSize>
<IdLocation>^HSAA.PatientD</IdLocation>
<IndexLocation>^HSAA.PatientI</IndexLocation>
<Property name="%%CLASSNAME"/>
<Property name="%%ID"/>
<Property name="AccountNumber"/>
<Property name="Address"/>
<Property name="Addresses"/>
<Property name="Age"/>
<Property name="AgeInMonths"/>
<Property name="Alias"/>
<Property name="Aliases"/>
<Property name="BirthDate"/>
<Property name="BirthGender"/>
<Property name="BirthOrder"/>
<Property name="BirthPlace"/>
<Property name="BirthTime"/>
<Property name="BlankNameReason"/>
<Property name="CarrierSpecificMemberID"/>
<Property name="Citizenship"/>
<Property name="Comments"/>
<Property name="CommunicationPreference"/>
<Property name="ContactInfo"/>
<Property name="CreatedBy"/>
<Property name="CreatedOn"/>
<Property name="CustomPairs"/>
<Property name="DeathDeclaredBy"/>
<Property name="DeathLocation"/>
<Property name="DeathTime"/>
<Property name="Deceased"/>
<Property name="EncounterNumber"/>
<Property name="EnteredAt"/>
<Property name="EnteredAt2"/>
<Property name="EnteredBy"/>
<Property name="EnteredOn"/>
<Property name="EthnicGroup"/>
<Property name="Ethnicity"/>
<Property name="Extension"/>
<Property name="ExternalId"/>
<Property name="FromTime"/>
<Property name="Gender"/>
<Property name="HSAAID"/>
<Property name="ImmunizationRegistryStatus"/>
<Property name="ImmunizationRegistryStatusEffectiveDate"/>
<Property name="InactiveMRNs"/>
<Property name="IsIdentityUnknown"/>
<Property name="IsMultipleBirth"/>
<Property name="IsProtected"/>
<Property name="LastUpdated"/>
<Property name="MPIID"/>
<Property name="MaritalStatus"/>
<Property name="MothersFullName"/>
<Property name="MothersMaidenName"/>
<Property name="Name"/>
<Property name="Occupation"/>
<Property name="OtherLanguages"/>
<Property name="PatientNumbers"/>
<Property name="PrimaryLanguage"/>
<Property name="PriorPatientNumbers"/>
<Property name="ProtectedEffectiveDate"/>
<Property name="PublicityCode"/>
<Property name="PublicityEffectiveDate"/>
<Property name="Race"/>
<Property name="Races"/>
<Property name="RawGender"/>
<Property name="Religion"/>
<Property name="SSNOrProxy"/>
<Property name="SetIsPregnant"/>
<Property name="SupportContacts"/>
<Property name="Tag"/>
<Property name="ToTime"/>
<Property name="UpdatedOn"/>
<SQLMap name="$Patient"/>
<SQLMap name="BirthDateIndex"/>
<SQLMap name="BirthTimeIndex"/>
<SQLMap name="CarrierSpecificMemberIDIndex"/>
<SQLMap name="DeathTimeIndex"/>
<SQLMap name="EnteredAt2Index"/>
<SQLMap name="EnteredAtIndex"/>
<SQLMap name="FamilyNameIndex"/>
<SQLMap name="GenderCodeIndex"/>
<SQLMap name="HSAAIDIndex"/>
<SQLMap name="IDKEY"/>
<SQLMap name="LastUpdatedIndex"/>
<SQLMap name="MPIIDIndex"/>
<SQLMap name="SSNOrProxyIndex"/>
<StreamLocation>^HSAA.PatientS</StreamLocation>
<Type>%Storage.Persistent</Type>
}

}
